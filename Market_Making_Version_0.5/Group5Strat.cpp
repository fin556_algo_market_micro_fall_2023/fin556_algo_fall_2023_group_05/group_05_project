#ifdef _WIN32
#include "stdafx.h"
#endif

#include "Group5Strat.h"


Group5Strat::Group5Strat(StrategyID strategyID,
    const std::string& strategyName,
    const std::string& groupName) :
    Strategy(strategyID, strategyName, groupName) {
}

Group5Strat::~Group5Strat() {
}

void Group5Strat::DefineStrategyParams() {
}

void Group5Strat::DefineStrategyCommands() {
}

void Group5Strat::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate) {
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, 10);
    }
    std::cout << "Registering for the strategy event" << std::endl;
}

void Group5Strat::OnTrade(const TradeDataEventMsg& msg) {
}

void Group5Strat::OnTopQuote(const QuoteEventMsg& msg) {
}  

void Group5Strat::OnQuote(const QuoteEventMsg& msg) {
    /* If BBO changed */
    if (msg.IsFromDepth()) {
        if(debug_3){
            cout << "OnQuote from Depth" << endl;
            debug_3--;
        }
        
        const Instrument* cur_instrument = &msg.instrument();
        double mid_price = (msg.instrument().top_quote().ask() + msg.instrument().top_quote().bid()) / 2;

        double bid_price = min(msg.instrument().top_quote().ask() - bid_ask_epsilon, mid_price - (spread / 2) - (TICK_SIZE * net_quantity / MAX_POSITION * inventory_factor));
        double ask_price = max(msg.instrument().top_quote().bid() + bid_ask_epsilon, mid_price + (spread / 2) - (TICK_SIZE * net_quantity / MAX_POSITION * inventory_factor));
        //The bid and ask quotes should shift to the same direction, so their symbols should both be minus
        int ask_quantity = 0;
        int bid_quantity = 0;

        int dynamic_base_quantity = base_quantity;
        int hour_index = msg.source_time().time_of_day().hours();
        if(previous_day_avg_spread_per_hour.find(hour_index) != previous_day_avg_spread_per_hour.end()) {
            dynamic_base_quantity = (dynamic_base_quantity * percentage_profit / (previous_day_avg_spread_per_hour[hour_index][1] / previous_day_avg_spread_per_hour[hour_index][0]));
            bid_price = min(msg.instrument().top_quote().ask() - bid_ask_epsilon, mid_price - ((previous_day_avg_spread_per_hour[hour_index][1] / previous_day_avg_spread_per_hour[hour_index][0]) / 2) - (TICK_SIZE * net_quantity / MAX_POSITION * inventory_factor));
            ask_price = max(msg.instrument().top_quote().bid() + bid_ask_epsilon, mid_price + ((previous_day_avg_spread_per_hour[hour_index][1] / previous_day_avg_spread_per_hour[hour_index][0]) / 2) - (TICK_SIZE * net_quantity / MAX_POSITION * inventory_factor));
        }

        if(net_quantity > 0) {
            ask_quantity = dynamic_base_quantity + abs(net_quantity);
            bid_quantity = dynamic_base_quantity;
        } else {
            bid_quantity = dynamic_base_quantity + abs(net_quantity);
            ask_quantity = dynamic_base_quantity;
        }

        ask_quantity *= -1;

        SendOrder(cur_instrument, ask_quantity, ask_price);
        SendOrder(cur_instrument, bid_quantity, bid_price);

        AdjustInventory(cur_instrument);
    }
}

void Group5Strat::OnDepth(const MarketDepthEventMsg& msg) {
    /* Update current day average spread calculation per hour */
    int hour_index = msg.source_time().time_of_day().hours();
    // Swap for new day
    if(hour_index < prev_update_hour_index) {
        previous_day_avg_spread_per_hour.clear();
        for(const auto& pair : current_day_avg_spread_per_hour) {
            previous_day_avg_spread_per_hour[pair.first] = pair.second;
        }
        current_day_avg_spread_per_hour.clear();
    }
    double update_spread = (msg.instrument().top_quote().ask() - msg.instrument().top_quote().bid());
    if(current_day_avg_spread_per_hour.find(hour_index) != current_day_avg_spread_per_hour.end()) {
        if(__DBL_MAX__ - update_spread < current_day_avg_spread_per_hour[hour_index][1] && __DBL_MAX__ - 1 < current_day_avg_spread_per_hour[hour_index][0]) {
            current_day_avg_spread_per_hour[hour_index][0]++;
            current_day_avg_spread_per_hour[hour_index][1] += update_spread;
        }
    } else {
        current_day_avg_spread_per_hour[hour_index] = {1.0, update_spread};
    }
    prev_update_hour_index = hour_index;
}

void Group5Strat::OnBar(const BarEventMsg& msg) {
}

void Group5Strat::OnMarketState(const MarketStateEventMsg& msg) {
}

void Group5Strat::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    if(debug_1) {
        std::cout << "name = " << msg.name() << std::endl;
        std::cout << "order id = " << msg.order_id() << std::endl;
        std::cout << "fill occurred = " << msg.fill_occurred() << std::endl;
        std::cout << "update type = " << msg.update_type() << std::endl;
        std::cout << "time " << msg.update_time() << std::endl;
        std::cout << "price " << msg.order().price() << std::endl;
        debug_1--;
    }

    const Order* cur_order = &msg.order();

    if(msg.fill_occurred() && open_orders_size_completed.find(msg.order_id()) != open_orders_size_completed.end()) {
        if(debug_4) {
            cout << "Fill Occurred: " << msg.order_id() << endl;
            debug_4--;
        }
        net_quantity -= (IsBuySide(cur_order->order_side()) ? 1 : -1) * (abs(cur_order->size_completed()) - open_orders_size_completed[cur_order->order_id()]);
        if(cur_order->size_remaining() == 0) {
            open_orders_size_completed.erase(cur_order->order_id());
        } else {
            open_orders_size_completed[cur_order->order_id()] = min(abs(cur_order->size_completed()), INT_MAX);
        }
    } else if (cur_order->order_state() == ORDER_STATE_CANCELLED) {
        net_quantity -= (IsBuySide(cur_order->order_side()) ? 1 : -1) * (abs(cur_order->size_remaining()));
        open_orders_size_completed.erase(cur_order->order_id());
    }
}

void Group5Strat::OnStrategyControl(const StrategyStateControlEventMsg& msg) {
}

void Group5Strat::OnResetStrategyState() {
}

void Group5Strat::OnDataSubscription(const DataSubscriptionEventMsg& msg) {
}

void Group5Strat::OnStrategyCommand(const StrategyCommandEventMsg& msg) {
}

void Group5Strat::OnParamChanged(StrategyParam& param) {
}

void Group5Strat::SendOrder(const Instrument* instrument, int trade_size, double trade_price) {
    /* Spoofing Binance Crypto Data as IEX APPL data for Strategy Studio preferences */
    OrderParams params( *instrument,
                        abs(trade_size),
                        trade_price,
                        MARKET_CENTER_ID_IEX,           
                        (trade_size > 0) ? ORDER_SIDE_BUY_TO_CLOSE : ORDER_SIDE_SELL_TO_CLOSE,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_LIMIT );

    TradeActionResult cur_trade_action = trade_actions()->SendNewOrder(params);

    if (cur_trade_action == TRADE_ACTION_RESULT_SUCCESSFUL) {
        if(debug_2) {
            cout << "Sent order: " << params.order_id << endl;
            debug_2--;
        }
        net_quantity += trade_size;
        open_orders_size_completed[params.order_id] = 0;
    }
}

void Group5Strat::AdjustInventory(const Instrument* instrument) {
    if(((double)net_quantity / MAX_POSITION) > inventory_threshold) {
        SendOrder(instrument, (-1 * take_quantity), instrument->top_quote().ask());
    } else if (((double)net_quantity / MAX_POSITION) < (-1 * inventory_threshold)) {
        SendOrder(instrument, take_quantity, instrument->top_quote().bid());
    }
}
