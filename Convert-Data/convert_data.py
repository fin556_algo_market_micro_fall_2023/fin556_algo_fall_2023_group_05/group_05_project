import csv
import gzip
import argparse
import os

def csv_gz_to_txt_gz(csv_gz_path, txt_gz_path):
    with gzip.open(csv_gz_path, 'rt') as csv_gz_file:
        reader = csv.reader(csv_gz_file)

        with gzip.open(txt_gz_path, 'wt') as txt_gz_file:
            for row in reader:
                print(row)
                txt_gz_file.write(','.join(row) + '\n')

def process_folder(input_folder, output_folder):
    for filename in os.listdir(input_folder):
        if filename.endswith('.csv.gz'):
            csv_gz_path = os.path.join(input_folder, filename)
            txt_gz_path = os.path.join(output_folder, filename.replace('.csv.gz', '.txt.gz'))
            csv_gz_to_txt_gz(csv_gz_path, txt_gz_path)
            print(f"Converted {filename} to {filename.replace('.csv.gz', '.txt.gz')}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert .csv.gz files to .txt.gz')
    parser.add_argument('--input_folder', type=str, help='Path to the input folder')
    parser.add_argument('--output_folder', type=str, help='Path to the output folder')

    args = parser.parse_args()

    process_folder(args.input_folder, args.output_folder)
