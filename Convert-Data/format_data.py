import gzip
import argparse
import os
import datetime


# Global counter
global_counter = 0

def generate_seq_num():
    global global_counter
    global_counter += 1
    return global_counter

def format_data(input_path, output_path):
    with gzip.open(input_path, 'rt') as txt_gz_file:
        with gzip.open(output_path, 'wt') as output_gz_file:
            first_line = True
            for line in txt_gz_file:
                if first_line:
                    formatted_line = 'COLLECTION_TIME,SOURCE_TIME,SEQ_NUM,TICK_TYPE,MARKET_CENTER,SIDE,ORDER_ID,PRICE,SIZE,MMID,REASON,\
                        OLD_ORDER_ID,OLD_ORDER_PRICE,PRIORITY_INDICATOR,IS_PARTIAL\n'
                    first_line = False
                else:
                    formatted_line = format_line(line)
                print(formatted_line)
                output_gz_file.write(formatted_line + '\n')

def format_line(line):
    exchange, symbol, timestamp, local_timestamp, is_snapshot, side, price, amount = line.strip().split(',')
    symbol = "BTCUSDT"
    collection_time = datetime.datetime.utcfromtimestamp(int(timestamp)/1e6).strftime('%Y-%m-%d %H:%M:%S.%f')
    source_time = datetime.datetime.utcfromtimestamp(int(timestamp)/1e6).strftime('%Y-%m-%d %H:%M:%S.%f')
    seq_num = generate_seq_num()
    tick_type = 'P'
    market_center = 'IEX'
    side = {'ask': 2, 'bid': 1}.get(side, 0)
    is_partial = {'true': 1, 'false': 0}.get(is_snapshot, 0)
    # feed_type = 3
    formatted_line = f"{collection_time},{source_time},{seq_num},{tick_type},{market_center},{side},{price},{amount},,,,{is_partial}\n"
    return formatted_line

def process_folder(input_folder, output_folder, start_date, end_date):
    for filename in os.listdir(input_folder):
        if filename.endswith('.txt.gz'):
            input_gz_path = os.path.join(input_folder, filename)
            date = "".join(filename.split("/")[-1].split("_")[4].split("-"))
            if date < start_date or date >= end_date:
                continue
            output_gz_path = os.path.join(output_folder, f"tick_AAPL_{date}.txt.gz")
            format_data(input_gz_path, output_gz_path)
            print(f"Processed {filename}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process .txt.gz files')
    parser.add_argument('--input_folder', type=str, help='Path to the input folder')
    parser.add_argument('--output_folder', type=str, help='Path to the output folder')
    parser.add_argument('--start_date', type=str, help='Start date for conversion')
    parser.add_argument('--end_date', type=str, help='End date for conversion')

    args = parser.parse_args()

    process_folder(args.input_folder, args.output_folder, args.start_date, args.end_date)
