'''
To run (can change output path) => 
python3 format_data.py Sample-data/incremental_book_L2/INPUT_DATA.csv Sample-data/ss_book_update_by_price/tick_SYMBOL_YYYYMMDD.txt
'''


''' Import modules '''
import argparse
import itertool
import os
import pandas as pd


''' Function to convert downloaded Binance data to the data format required by Strategy Studio '''
def convert_format(input_file, output_file):
    # Check if the input file exists
    if not os.path.isfile(input_file):
        print(f"Error: Input file '{input_file}' does not exist.")
        return

    # If the output file already exists, return: conversion already done <- could change to always convert
    if os.path.isfile(output_file):
        print(f"Output file '{output_file}' already exists.")
        return

    # Create the output directory if it doesn't exist
    output_directory = os.path.dirname(output_file)
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    '''
    Below section might be unnecessary.
    '''
    # Create the output file if it doesn't exist
    if not os.path.isfile(output_file):
        with open(output_file, 'w'):
            pass
        print(f"Output file '{output_file}' created.")

    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(input_file)

    # Create a new DataFrame with correct formatting (formatting specified by Strategy Studio's Text Tick Reader)
    new_df = pd.DataFrame({
        'COLLECTION_TIME': df['local_timestamp'],
        'SOURCE_TIME': df['timestamp'],
        'SEQ_NUM': 0,
        'TICK_TYPE': 'P',
        'MARKET_CENTER': 'binance-futures',
        'SIDE': df['side'],
        'PRICE': df['price'],
        'SIZE': df['amount'],
        'NUM_ORDERS': '',
        'IS_IMPLIED': '',
        'REASON': '',
        'IS_PARTIAL': ''
    })

    # Write the new DataFrame to the output text file without indexing and header
    new_df.to_csv(output_file, index=False, header=False)

    print(f"Conversion complete. The result is saved in {output_file}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert CSV file format.')
    parser.add_argument('input_file', help='Path to the input CSV file')
    parser.add_argument('output_file', help='Path to the output text file')
    args = parser.parse_args()

    convert_format(args.input_file, args.output_file)
