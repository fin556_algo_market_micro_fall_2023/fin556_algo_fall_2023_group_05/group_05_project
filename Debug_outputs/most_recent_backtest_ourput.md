rant@group05vm:~/ss/sdk/RCM/StrategyStudio/examples/strategies/Group5Strat$ make run_backtest
ls -l ./provision_scripts/
total 28
-rwxr-xr-x 1 vagrant vagrant  630 Dec  9 03:25 build_strategy.sh
-rwxr-xr-x 1 vagrant vagrant 1546 Dec  9 03:25 delete_instance.py
-rwxr-xr-x 1 vagrant vagrant  392 Dec  9 03:25 git_pull.sh
-rwxr-xr-x 1 vagrant vagrant 8348 Dec 13 17:27 results_analytics.py
-rwxr-xr-x 1 vagrant vagrant 2208 Dec  9 03:25 run_backtest.sh
pypy3 ./provision_scripts/delete_instance.py "Group5Strat" "Group5Strat"
Python script located at: /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/Group5Strat
Python script navigated to: /home/vagrant/ss/bt/db
Deleting all instances of all strategies from the sqlite database.
Displaying the contents of StrategyInstance & StrategyType tables: (should be empty if deleted successfully)
[]
[]
rm -rf *.o Group5Strat.so
g++ -c -fPIC -fpermissive -O3 -std=c++11 -I/usr/include -I/home/vagrant/ss/sdk/RCM/StrategyStudio/includes Group5Strat.cpp -o Group5Strat.o
g++ -shared -Wl,-soname,Group5Strat.so.1 -o Group5Strat.so Group5Strat.o /home/vagrant/ss/sdk/RCM/StrategyStudio/libs/x64/libstrategystudio_analytics.a /home/vagrant/ss/sdk/RCM/StrategyStudio/libs/x64/libstrategystudio.a /home/vagrant/ss/sdk/RCM/StrategyStudio/libs/x64/libstrategystudio_transport.a /home/vagrant/ss/sdk/RCM/StrategyStudio/libs/x64/libstrategystudio_marketmodels.a /home/vagrant/ss/sdk/RCM/StrategyStudio/libs/x64/libstrategystudio_utilities.a /home/vagrant/ss/sdk/RCM/StrategyStudio/libs/x64/libstrategystudio_flashprotocol.a
./provision_scripts/build_strategy.sh "Group5Strat" "Group5Strat" "AAPL"
Quitting server. This is to just to have a clean start.
Ignore any errors that may occur here because of quitting the server.

Quitting.
Connection id 1 finished.
Starting server. There should be no errors after this point.
Initializing Strategy Studio Backtesting Server...
Initializing Strategy Server...
PROCESSOR_LOG_LEVEL configured as DEBUG
Strategy Studio Server v 3.4.0.0
Protocol Version v21.0
Recovery mode disabled.
Loading corporate action adapter dll /home/vagrant/ss/bt/./corporate_action_adapters/TextCorporateActionAdapter.so...
Loading instrument information from reference database...
Loading equity instruments...
Finished loading 0 equity instruments from reference database.
Loading futures instruments...
Finished loading 0 futures instruments from reference database.
Loading options instruments...
Finished loading 0 option instruments from reference database.
Loading spot currency pair instruments...
Finished loading 0 spot currency pairs from reference database.
Loading bond instruments...
Finished loading 0 bond instruments from reference database.
Finished loading instrument information from reference database.
Starting Seq Num = 1
Loading strategy dll /home/vagrant/ss/bt/strategies_dlls/Group5Strat.so...
Found new strategy type info for strategy Group5Strat
Loaded strategy type Group5Strat from strategy DLL.
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/NomuraTickReader.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/OneTickAdapter.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/TextTickReader.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/LMAXAdapter.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/TickDataDotComAdapter.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/NanexAdapter.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/MorningstarTickAdapter.so...
Loading backtest reader library /home/vagrant/ss/bt/./backtester-readers/KDBAdapter.so...
Application state changed to STATE_APP_INITIALIZED
Strategy Server initialized...
Starting Strategy Studio Backtesting Server...
Starting Strategy Server...
Application state changed to STATE_APP_STARTING
Application state changed to STATE_MARKET_OPENING
Application state changed to STATE_RUNNING
Strategy Server started...
Running Strategy Studio Backtesting Server...Acceptor thread started.

Command line disabled. Type CTRL-C to end.
Started server
Login received from connection id 0
Strategy Group5Strat added to processor 0.

Quitting.
Connection id 0 finished.
Connection id 1 finished.
Created instance
chmod +x ./provision_scripts/run_backtest.sh
./provision_scripts/run_backtest.sh "Group5Strat" "2023-08-20" "2023-08-20"
/home/vagrant/ss/bt
Sleeping for 2 seconds while waiting for strategy studio to boot
StrategyServerBacktesting  bin                        license.txt    preferred_feeds.csv  strategy-daily
backtester-readers         corporate_action_adapters  logs           pstore               strategystudio.dat
backtester_config.txt      data_adapters              orderid.txt    strategies_dlls      text_tick_data
backtesting-results        db                         pnl_snapshots  strategy-conf        utilities
Server already running from current location.
/home/vagrant/ss/bt/utilities
StrategyCommandLine  cmd_config_example.txt  command_line_log_20231210.txt
cmd_config.txt       command_line_log.txt    command_line_log_20231213.txt
Login received from connection id 1
Starting backtest experiment 1 running 1 strategy with start date 2023-Aug-20 and end date 2023-Aug-20
DataSourceMgr::BuildDataSource: Received request to build data source BacktesterReader for processor id 1
Registering for the strategy event
Started backtest experiment 1 with start date 2023-Aug-20 and end date 2023-Aug-20

Quitting.
Connection id 1 finished.
Connection id 1 finished.
Last line found:,
Sleeping for 10 seconds...
run_backtest.sh completed
Latest CRA file path:, /home/vagrant/ss/bt/backtesting-results/BACK_Group5_2023-12-13_175220_start_08-20-2023_end_08-20-2023.cra
/home/vagrant/ss/bt/utilities
StrategyCommandLine  cmd_config_example.txt  command_line_log_20231210.txt
cmd_config.txt       command_line_log.txt    command_line_log_20231213.txt
Login received from connection id 2

export successful

Quitting.
Connection id 1 finished.
Connection id 2 finished.
./provision_scripts/run_backtest.sh: line 42: cd: /groupstorage/group05/group_05_project/Strategy/provision_scripts: No such file or directory
python3: can't open file 'results_analytics.py': [Errno 2] No such file or directory
Python script did not update backtest_data.json.