# group_05_project

## Final Report
**The final report for this project is [here](/Final_Report/final_report.md)**

## Teammates
### Vishesh Prasad (vprasad3@illinois.edu)
Hi! I am Vishesh Prasad and I am currently a junior studying Computer Engineering at the University of Illinois at Urbana-Champaign with minors in 
Math, Statistics, and Econometrics. I am a U.S. citizen, graduating in May 2025, and am interested in quantitative trading and quantitative research roles, and graduate school. I have experience working on statistical anlysis, research, and machine learning with a passion for financial markets. I am proficient in languages such as C++, Python, and others and have experience thorugh previous roles as an Undergraduate Research Assistant and an Software Developer Intern.
You can reach me through email or [Linkedin](https://www.linkedin.com/in/visheshprasad/)

### Zhicheng Tang (zt17@illinois.edu)
I am Zhicheng Tang,  currently pursuing a Master's degree in Industrial Engineering at the University of Illinois at Urbana-Champaign. My university study covers math, statistics, computer science, economics and finance. I have practical experiences in data analysis, quantitative research, signal generating, machine learning, and arbitrage strategy development. I am proficient in multiple programming languages such as Python, C++, R, SQL and MATLAB. Presently, I am gaining hands-on experiences as a quant developer intern at a California-based hedge fund. Prior to this, I have had the opportunity to work with leading tech companies in China including Huawei, Baidu and Tencent. 

I am seeking opporunities for May 2024 graduation. Feel free to reach out to me through email or [LinkedIn](https://www.linkedin.com/in/zhichengtang/)


### Yibang Tong (ybtong2@illinois.edu)
Yibang Tong is a junior student at uiuc and major in Mathematics. His estimated graduate time is Dec 2024, and currently seeking internship opportunities for quant trader/researcher. Linkedin profile: https://www.linkedin.com/in/yibang-tong/

### Harsh Agarwal (harsha4@illinois.edu)
I am a senior majoring in Computer Science. I am passionate about software development, particularly developing low latency systems for use in finaicial markets. I am well versed with core software development tools including data structures, algorithms, system design, distributed systems, and computer architecture. You can find me here on LinkedIn - https://www.linkedin.com/in/harshagl2002/

