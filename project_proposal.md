# FIN 556 Group 5 PROJECT PROPOSAL Fall 2023
Members:
- Vishesh Vidya Prasad (vprasad3)
- Yibang "Marco" Tong (ybtong2)
- Zhicheng Tang (zt17)
- Harsh Agarwal (harsha4)


- - - - 
## 1 - 4. Which specifically technologies, libraries, frameworks, languages, etc, you intend to use or investigate using for your project? What specific assets, symbols, etc you intend to trade? Where you intend to source the data sources for this? What date ranges you intend to backtest on and why

For our trading project, our primary programming languages are Python and C++. Python will serve as the backbone for our data processing and signal generation pipeline, utilizing libraries such as pandas, numpy, SciPy, Sklearn, statsmodels, matplotlib, torch, and tensorflow. In parallel, C++ will be employed for the actual trading strategies. To manage the large dataset effectively, we will leverage AWS for data processing and signal testing. Our asset focus will be on the largest cryptocurrencies in the universe—BTC. Our data source comes from a company that has generously offered to provide us with a variety of data types, including book snapshots, derivative tickers, spot tickers, quotes, trades, and liquidation data. Finally, we plan to backtest over a small time period now, and over a larger time period if we have the time.


- - - - 
## 5. What frameworks, libraries, etc, you will use for CI/CD (automated testing, deployment, etc)?

Most of the coding shall be done in Python and C++. In Python, we shall be using libraries such as NumPy, Pandas, Scipy, Scikit-learn, Matplotlib, for numerical analysis, cleaning data and generating and analyzing machine learning models/signals. We will use C++ to implement and generate signal strategies.


- - - - 
## 6. An extremely detailed finite state machine breaking down the exact logic your strategy, factoring in different movements in the market and as a function of time. Consider what you will do at the end of each trading day as well (do you hold overnight, close out all positions before the close, etc). Also factor in when placing various orders, how exactly are you determining both the price at which to submit limit orders and also for what total size / notational value (initially this can be simple but start thinking through these variables).

Finite state machine of our logic: (High overview:)
- Data pipeline -> generate signals -> strategy

Specifics:
- Data pipeline: Clean up data such that we have snapshots of the orderbook and
instrument tickers, quotes, and trades without gaps, for the top 5/10
cryptocurrencies on Binance (according to total volume traded).
- Generate signals: We look towards specific machine learning signal generation
and more simple math model signals
- Strategy: Market neutral strategy to minimize risk (hold both short and long, and
not only one). For placing specific orders, we use the machine learning signals to look at predicted prices which will influence the specific trades that we place. For each specific price, we might place orders at multiple layers at each to get an average price and reduce risk. The size of each order will also depend on the current size on the market book, and the specific strategies that we would impolent after looking at the signals we generate (market making strategies and normal trading strategies are some factors which would influence this decision). We will also tune the fee structures/commissions and the latency on Strategy Studio when we look at strategy and examine how our PnL changes.


- - - - 
## 7. Detailed timeline of what needs to be done and when to complete your project

For this project, we are given 7 weeks (October 28, 2023 - December 16, 2023) and we break it down into three phases.
- Phase 1 deals with cleaning up the data that we have acquired and porting over said data into strategy studio. We define cleaning up the data as making sure there are no gaps in the orderbook, trades, and ticker data at the smallest possible granularity, since the data we have acquired from Binance has snapshots marked at varying timestamps. We port small snapshots of our data into strategy studio to ensure that we are correctly formatting the data such that it can be read correctly. Allowing us to
successfully generate signals in phase 2. Since phase 1 is a big part of the project, we plan to spend 2 1⁄2 to 3 weeks.
- For phase 2, we are implementing the generation of our different signals in C++ and testing it in Strategy Studio. We plan for phase 2 to take around 2 weeks for a correct implementation.
- Our last and final phase, phase 3, is where we combine our previously implemented signal generators into a strategy, looking at specific market making or trading and trying to generate alpha. Here we fine tune specific parameters and test our different signals to either trade high volume, or return a profit. We expect this final phase to take around 2 weeks.


- - - - 
## 8. Breakdown of 1: bare minimum target for your project to not be a failure, 2: expected completion goals, and 3. "Reach" goals (more advanced work that, given enough time, your team hopes to also accomplish)

The bare minimum for this project is to build a pipeline which gets and cleans the data and then generate signals accordingly. The expected completion goals shall be to perform market making and also generate signals based on Machine Learning models. The ultimate goal is to try and generate profits by implementing the strategies in the actual market


- - - - 
## 9. Detailed specification of what each individual team member will be contributing to the project (and ensuring this aligns with everyone's actual skills or ability to learn new skills).

Particularly, the entire team will work on parsing and cleaning the data to get a better understanding of the data. One person shall then try and work on strategy studios to run the data using example strategies. The other team members will continue to work on generating the data for future use. Upon completion of this step, we shall move into teams of two to use snapshots to generate signals which will then be used to implement long-short strategies.


- - - - 
## 10. Any external resources your project may depend on or require (including anything you need from me).

We will use crypto data from Binance (fetched by Zhicheng). If we find that this data is not compatible with Strategy Studio or have other issues, we may turn to IEX data instead.


- - - - 
## 11. What are the final deliverable(s) of your project

We will submit a detailed report showing our whole project in gitlab. If we have available time ( including after this semester), we will work on our “Reach” goal, profitably.


- - - - 
## 12. Graduation and grad school plans

- Vishesh Vidya Prasad - Graduating spring 2025. Interested in grad school, specifically Masters or PhD in computer science.
- Yibang "Marco" Tong - Graduating fall 2024.
- Zhicheng Tang - Graduating spring 2024.
- Harsh Agarwal - Graduating spring 2024. Pursuing an online masters from UIUC


- - - - 
