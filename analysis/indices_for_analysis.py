#Once we have the backtesting results, just type the three correct file into "read_csv(111111)"

import pandas as pd
import matplotlib.pyplot as plt


#1. Analyze pnl.csv
pnl = pd.read_csv(f"BACK_Group5Strat_2023-12-20_020031_start_08-08-2023_end_08-21-2023_pnl.csv")  ###

#plot
pnl['Cumulative PnL'].plot()
plt.title('PnL Plot')
plt.xlabel('Time')
plt.ylabel('PnL')
plt.show()

#Return
print("Return:", pnl['Cumulative PnL'].iloc[-1] - pnl['Cumulative PnL'].iloc[0])

#Sharpe Ratio
pnl['PnL_each_time'] = pnl['Cumulative PnL'].diff().fillna(0)
rate = 0.0423 / 252  #10 Year Treasury Rate is 4.23% (By Dec 8 https://ycharts.com/indicators/10_year_treasury_rate)
pnl['Excess Returns'] = pnl['PnL_each_time'] - rate
sharpe_ratio = pnl['Excess Returns'].mean() / pnl['Excess Returns'].std()
print("Sharpe Ratio:", sharpe_ratio)

#Sortino Ratio
downside_deviation = pnl[pnl['Excess Returns'] < 0]['Excess Returns'].std()
sortino_ratio = pnl['Excess Returns'].mean() / downside_deviation
print("Sortino Ratio:", sortino_ratio)

#Maximum Drawdown
pnl['Drawdown_each_time'] = pnl['Cumulative PnL'] - pnl['Cumulative PnL'].cummax()
max_drawdown = pnl['Drawdown_each_time'].min()
end_time = pnl['Drawdown_each_time'].idxmin()
begin_time = pnl['Cumulative PnL'].loc[:end_time].idxmax()
print("Maximum Drawdown:", max_drawdown)
print("Start Time of Maximum Drawdown:", pnl['Time'].iloc[begin_time])
print("End Time of Maximum Drawdown:", pnl['Time'].iloc[end_time])



#2. Analyze order.csv and fill.csv
order = pd.read_csv(f"BACK_Group5Strat_2023-12-20_020031_start_08-08-2023_end_08-21-2023_order.csv")
fill = pd.read_csv(f"BACK_Group5Strat_2023-12-20_020031_start_08-08-2023_end_08-21-2023_fill.csv")
Cancelled_order = [i for i in range(order.shape[0]) if order['State'].iloc[i] == 'CANCELLED']
Cancelled_rate = len(Cancelled_order)/order.shape[0]
print("Number of orders canceled:", len(Cancelled_order))
print("Percentage of orders canceled:", Cancelled_rate)
#Show how much of our quotes are cancelled (Because of GTD)

print("The total transaction fee is:", order["ExecutionCost"].sum() + fill["ExecutionCost"].sum())
#
